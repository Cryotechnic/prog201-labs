public class Rectangular {


    /* 
     * A simple class for rectangular
     * @author Ron Friedman
     * @version 1.0.0
    */




    // default constructor -> width = length = 1
    // cpnstructor with width and length
    // toString     : Rectangular width and length
    // equals   widths are equal and lengths are equal
    // getter setter

    // special methods:
    // calcArea()
    // calcPerimeter()

    private double width;
    private double length;

     /**
     * Default constructor with width = 1 and length = 1
     */
    public setVars() {
        this.width = 1;
        this.length = 1;
    }
        /**
         * Constructor with width and length
         * @param width as the width of rectangular
         * @param length as the length of rectangular
         */
        public myConst(double width, double length) {
            this.width = width;
            this.length = length;
        }

        @Override 
        public String toString() {
            return String.format("Width: %.2f\nLength:%.2f!\n", width, length);
        }

        public boolean equals(Rectangular rec) {
            return this.width == rec.width && this.length == rec.length;
            // Any operator with more than 3 values should contain spaces
        }
        public void getWidth() {
            return width;
        }

        public void getLength() {
            return length;
        }

        public double setWidth(double width) {
            return this.width = width;
        }

        public double setLength(double length) {
            return this.length = length;
        }
}